//
//  Home.swift
//  booterapss
//
//  Created by JUAN DIAZ on 21/04/16.
//  Copyright © 2016 JUAN DIAZ. All rights reserved.
//

import UIKit

class Home: UITableViewController, SWRevealViewControllerDelegate {
    

    @IBOutlet weak var menu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menu.target = self.revealViewController()
            menu.action = Selector("revealToggle:")
            self.revealViewController().delegate = self
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}