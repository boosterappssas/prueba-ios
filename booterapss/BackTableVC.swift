//
//  BackTableVC.swift
//  booterapss
//
//  Created by JUAN DIAZ on 21/04/16.
//  Copyright © 2016 JUAN DIAZ. All rights reserved.
//

import Foundation

class BackTableVC: UITableViewController {
    
    var TableArray = [String]()
    var TableArrayImage = [String]()

    @IBOutlet weak var btn_logout: UIButton!
    @IBOutlet var tableMenu: UITableView!
    override func viewDidLoad() {
        TableArray = ["Home","Explore","My Recipes","Activity","Settings"]
        TableArrayImage = ["home.png","explore.png","myre.png","activity.png","settings.png"]
        

        
        let cornerRad = CGFloat (03.0)
        btn_logout.layer.cornerRadius = cornerRad
        
        
        
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SlideCellTableViewCell
        
        cell.label_menu.text = self.TableArray[indexPath.row]
        cell.icon_menu.image = UIImage(named: TableArrayImage[indexPath.row])
        return cell
    }
}
