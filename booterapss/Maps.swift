//
//  Maps.swift
//  booterapss
//
//  Created by JUAN DIAZ on 20/04/16.
//  Copyright © 2016 JUAN DIAZ. All rights reserved.
//
import UIKit
import GoogleMaps

class Maps: UIViewController {
    
    var latitud = 0.0
    var longitud = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "Arial", size: 12)!
        ]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        
        
        print(self.latitud, self.longitud)
        let camera = GMSCameraPosition.cameraWithLatitude(self.latitud, longitude: self.longitud, zoom: 8)
        let mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        self.view = mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(self.latitud, self.longitud)
        marker.appearAnimation = kGMSMarkerAnimationPop
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
        marker.icon = UIImage(named:"Map_marker.png")
        marker.map = mapView
    }
}