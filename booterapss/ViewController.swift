//
//  ViewController.swift
//  booterapss
//
//  Created by JUAN DIAZ on 19/04/16.
//  Copyright © 2016 JUAN DIAZ. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var menu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menu.target = self.revealViewController()
        menu.action = Selector("revealToggle:")
        
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

