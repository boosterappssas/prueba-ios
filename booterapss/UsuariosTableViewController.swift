//
//  UsuariosTableViewController.swift
//  pruebaBoosterApps
//
//  Created by Charlie Molina on 20/04/16.
//  Copyright © 2016 Indibyte. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KFSwiftImageLoader



class UsuariosTableViewController: UITableViewController, SWRevealViewControllerDelegate  {

    var allObjects = [Usuario]()
    var elements = [Usuario]()
    
    
    var currentPage = 0
    var nextpage = 0
    var itemsPage = 8
    
    @IBOutlet weak var menu: UIBarButtonItem!


    override func viewWillAppear(animate:Bool) {
        super.viewWillAppear(animate)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backItem = UIBarButtonItem(title: "Back", style: .Bordered, target: nil, action: nil)
    
        navigationItem.backBarButtonItem = backItem
        
        if self.revealViewController() != nil {
            menu.target = self.revealViewController()
            menu.action = Selector("revealToggle:")
            self.revealViewController().delegate = self
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let attributes = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: UIFont(name: "Arial", size: 12)!
            ]
            self.navigationController?.navigationBar.titleTextAttributes = attributes
           
           
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

            Alamofire.request(.GET, "http://cuidemoseltolima.co/api/v1/reportes/reportes.json").validate().responseJSON { response in
                switch response.result {
                case .Success:

                    if let value = response.result.value {
                        let json = JSON(value)
                        
                        for (_,subJson):(String, JSON) in json {
                            
                            self.allObjects.append(Usuario(json: subJson))
                            
                        }
//                        let data = [Usuario](self.allObjects[0...self.itemsPage])
//                        for u in data{
//                            self.elements.append(u)
//                        }
                    }
                    dispatch_async(dispatch_get_main_queue(),{
                        print(self.allObjects.count)
                        self.tableView.reloadData()
                    })
                case .Failure(let error):
                    print(error)
                }
            
            
        }

        
      
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.allObjects.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("CELL", forIndexPath: indexPath) as! OurTableViewCell
        
        let user = self.allObjects[indexPath.row]
        cell.tipo.text = user.tipo
        cell.municipio.text = user.municipio
        cell.nombre.text = user.nombre
        cell.foto.loadImageFromURLString(user.url_imagen, placeholderImage: UIImage(named: "placeholder.png")) {
            (finished, potentialError) in
            
            if finished {
                // Do something in the completion block.
            }
            else if let error = potentialError {
                print("error occurred with description: \(error.localizedDescription)")
            }
        }
        
        
        
        
        return cell

    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // self.performSegueWithIdentifier("maps", sender: indexPath)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        

//        nextpage = elements.count - 5
//        if indexPath.row == nextpage {
//            currentPage++
//            nextpage = elements.count - 5
//                    print(currentPage)
//            let data = [Usuario](self.allObjects[currentPage...self.itemsPage])
//            for u in data {
//                 self.elements.append(u)
//            }
//            tableView.reloadData()
//        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
//     MARK: - Navigation

  //   In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "maps"){
             let  destination = segue.destinationViewController as! Maps
            let path = self.tableView.indexPathForSelectedRow
                destination.longitud = self.allObjects[(path?.row)!].longitud
                destination.latitud = self.allObjects[(path?.row)!].latitud
            
        }
    }
    



}
