//
//  OurTableViewCell.swift
//  pruebaBoosterApps
//
//  Created by Charlie Molina on 20/04/16.
//  Copyright © 2016 Indibyte. All rights reserved.
//

import UIKit

class OurTableViewCell: UITableViewCell {


    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var municipio: UILabel!
    @IBOutlet weak var tipo: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
