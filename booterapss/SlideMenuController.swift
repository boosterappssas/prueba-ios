//
//  SlideMenuController.swift
//  booterapss
//
//  Created by JUAN DIAZ on 20/04/16.
//  Copyright © 2016 JUAN DIAZ. All rights reserved.
//

import Foundation
import SlideMenuControllerSwift

class ContainerViewController: SlideMenuController {
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewControllerWithIdentifier("Main") {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewControllerWithIdentifier("Left") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }
    
}
