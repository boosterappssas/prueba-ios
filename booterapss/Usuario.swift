//
//  Usuario.swift
//  pruebaBoosterApps
//
//  Created by Charlie Molina on 20/04/16.
//  Copyright © 2016 Indibyte. All rights reserved.
//

import Foundation
import SwiftyJSON

class   Usuario {

    var url_imagen: String!
    var nombre: String!
    var email: String!
    var latitud: Double!
    var longitud: Double!
    var municipio: String!
    var tipo: String!
    
    required init(json: JSON) {

        url_imagen = json["url_imagen"].stringValue
        nombre = json["nombre"].stringValue
        email = json["email"].stringValue
        latitud = json["latitud"].doubleValue
        longitud = json["longitud"].doubleValue
        municipio = json["municipio"].stringValue
        tipo = json["tipo"].stringValue
    }
    
    
}